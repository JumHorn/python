# Python

learning notes for python


# basic config
## python vscode tab config
1. ctrl+shift+p
2. 执行命令Preferences: Configure Language Specific Settings
3. 选择python
4. 添加如下配置
```json
"[python]": {
  "editor.insertSpaces": true,
  "editor.tabSize": 4
}
```

## autopep8格式化

autopep8格式化需要设置参数，才能全部格式化

1. vscode ==> settings search autopep8
2. autopep8 args add items **--aggresive**

## pip配置镜像

~/.pip/pip.conf
```ini
[global]
index-url = https://pypi.tuna.tsinghua.edu.cn/simple
```

## 安装pip
```shell
easy_install pip3
```

# Debug
1. pdb
	python -m pdb app.py arg1 arg2
2. gdb
	gdb python
	(gdb) run app.py

# 编程语言学习基础
* 输入输出
* 数据结构(set map priority_queue heap stack)
* 算法(常用算法的实现)
* 日期时间
* 线程
* 文件系统
* 网络
* 系统状态(IP Mac StorageInfo memory cpu)
* 系统调用(以及调用系统命令)
* 语言特性(装饰器，迭代器等)


# 其他
## 实现文件服务器

1. python3
> python -m http.server -p 80

2. python2
> python -m SimpleHTTPServer -p 80
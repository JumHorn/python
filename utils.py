# -*- coding: UTF-8 -*-


# vocabulary
NUM = ["zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten",
       "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen", "twenty"]

WEEK = [
    "Monday",
    "Tuesday",
    "Wednesday",
    "Thursday",
    "Friday",
    "Saturday",
    "Sunday"]

MONTH = [
    "January",
    "February",
    "March",
    "April",
    "May",
    "June",
    "July",
    "August",
    "September",
    "October",
    "November",
    "December"]


def isPrimer(n):
    if n < 2:
        return False
    if n == 2:
        return True
    for i in range(2, n + 1):
        if i * i > n:
            break
        if n % i == 0:
            return False
    return True

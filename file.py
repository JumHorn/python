# -*- coding: UTF-8 -*-

import glob

f = open('main.py')

# read file
line = f.readline()
while line != "":
    print(line)
    line = f.readline()

# write file
with open('spam.txt', 'w') as file:
    file.write('Spam and eggs!')

# find file
l = glob.glob("*.py")
print(l)

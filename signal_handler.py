# -*- coding: UTF-8 -*-

import signal
import sys
import time

# 系统signal


def handler(sinnum, frame):
    print('goodbye world')
    sys.exit()


def Start():
    signal.signal(signal.SIGINT, handler)
    while True:
        print('hello world')
        time.sleep(1)


Start()

# 模拟qt signal


class Signal:
    def __init__(self):
        self.slot = []

    def emit(self, *arg, **kw):
        for handler in self.slot:
            handler(arg, kw)

    def connect(self, handler):
        self.slot.append(handler)

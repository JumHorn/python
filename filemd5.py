# -*- coding: UTF-8 -*-

import hashlib  # for md5


def getfilemd5(filename):
    md5obj = hashlib.md5()
    with open(filename, 'rb') as f:
        while True:
            data = f.read(1024)
            if not data:
                break
            md5obj.update(data)

    return md5obj.hexdigest()

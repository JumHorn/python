# -*- coding: UTF-8 -*-

from udpsocket import *
import os
# 被运行的文件__name__是,其他是导入的文件名,但是导入文件名为__main__却是无效
print(__name__)

l = [1, 2, 3]

# 强制类型转换
d = 12
print(str(d))

print(type(l))
print(type(tuple(l)))

# dir 查看模块内置函数和变量
dir(os)

# -*- coding: UTF-8 -*-

import time

# 可以用于程序计时
start_time = time.time()
time.sleep(2)
end_time = time.time()
print("time elasped:", end_time-start_time)

print(time.localtime())
time.sleep(5)
print(time.localtime())

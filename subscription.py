# -*- coding: UTF-8 -*-

from urllib.request import urlopen
from base64 import b64decode


def subscribeurlparse(subscribe_url):
    return_content = urlopen(subscribe_url).read()
    share_links = b64decode(return_content).decode('utf-8').splitlines()
    # print(share_links)
    return share_links


if __name__ == "__main__":
    subscribe_url = 'https://sub.paasmi.com/subscribe/56922/590tlRoXvKce?mode=2'
    print(subscribeurlparse(subscribe_url))

# -*- coding: UTF-8 -*-

from socket import *


def StartServer():
    # '0.0.0.0' IP::any
    ip_port = ('127.0.0.1', 23358)

    sk = socket(AF_INET, SOCK_DGRAM)
    sk.bind(ip_port)

    # 加入到组播
    sk.setsockopt(IPPROTO_IP, IP_ADD_MEMBERSHIP, inet_aton(
        '239.0.0.1') + inet_aton('127.0.0.1'))

    while True:
        client_data, addr = sk.recvfrom(1024)
        print(addr, client_data)
        # print client_data.encode('hex')


def StartClient():
    ip_port = ('239.0.0.1', 23358)
    sk = socket(AF_INET, SOCK_DGRAM)
    return sk.sendto("hello", ip_port)


if __name__ == "__main__":
    s = input('--> ')  # 输入字符串不需要自己加引号
    if s == 's':
        StartServer()
    elif s == 'c':
        print(StartClient())

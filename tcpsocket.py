# -*- coding: UTF-8 -*-

import socket


def StartServer():
    ip = ('0.0.0.0', 64219)
    sk = socket.socket()
    sk.bind(ip)
    sk.listen(5)

    conn, addr = sk.accept()

    while True:
        data = conn.recv(1024)
        print(data)


def StartClient():
    ip = ('127.0.0.1', 64219)
    sk = socket.socket()
    sk.connect(ip)
    sk.send('hello world')

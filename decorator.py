# -*- coding: UTF-8 -*-

class A:
    def foo(self, x):
        print("executing foo(%s,%s)" % (self, x))

    @classmethod   # cls传递类型(type),self传递对象(instance)
    def class_foo(cls, x):
        print("executing class_foo(%s,%s)" % (cls, x))

    @staticmethod
    def static_foo(x):
        print("executing static_foo(%s)" % (x))


a = A()
a.foo(1)
a.class_foo(1)
a.static_foo(1)

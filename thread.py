# -*- coding: UTF-8 -*-


# 1. 给线程传递函数
# 2. 继承线程类，重写run方法

# Lock线程同步

import threading
L = threading.Lock()


def printMsg():
    L.acquire()
    for i in range(5):
        print('hello world', i)
    L.release()


t1 = threading.Thread(None, printMsg)
t2 = threading.Thread(None, printMsg)
t1.start()
t2.start()

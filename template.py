# -*- coding: UTF-8 -*-
import math

# const variables


MOD = int(1e9 + 7)

# board dfs direction
path = [-1, 0, 1, 0, -1]

# frequently used functions


def gcd(x, y):
    return y if x == 0 else gcd(y % x, x)


def bitCount(n):
    return bin(n).count('1')


# pow with mod
def modPow(x, n, mod=int(1e9 + 7)):
    return math.pow(x, n, mod)


# palindrome generator
def generatePalindrome():
    len = 0
    while True:
        len += 1
        x = int(math.pow(10, (len - 1) // 2))
        y = int(math.pow(10, (len + 1) // 2))
        for i in range(x, y):
            val, j = i, i // 10 if len % 2 == 1 else i
            while j > 0:
                val = val * 10 + j % 10
                j //= 10

            yield val


# factorial stuff
def produceFactorial(n):
    # Pre-process fac and inverse fac.
    fac, ifac = [1] * (n + 1), [1] * (n + 1)
    for i in range(2, n + 1):
        fac[i] = fac[i - 1] * i % MOD
        ifac[i] = modPow(fac[i], MOD - 2)


# combination table
def createCombinationTable(N):
    combination = [[0] * (N + 1) for _ in range(0, N + 1)]
    combination[0][0] = 1
    for i in range(1, N + 1):
        combination[i][0] = combination[i][i] = 1
        for j in range(1, i // 2 + 1):
            combination[i][j] = combination[i][i - j] =         \
                combination[i - 1][j] + combination[i - 1][j - 1]


# kmp next array
def createKMP(pattern):
    N = len(pattern)
    res = [0] * N
    j = 0
    for i in range(1, N):
        while j > 0 and pattern[i] != pattern[j]:
            j = next[j - 1]
        if pattern[i] == pattern[j]:
            j += 1
        res[i] = j
    return res


# dijkstra
def dijkstra():
    pass


# divide and conquer
def merge(arr, dup, first, mid, last):
    i, j, d = first, mid, 0
    while i < mid or j < last:
        if i == mid:
            dup[d] = arr[j]
            j += 1
        elif j == last:
            dup[d] = arr[i]
            i += 1
        else:
            if arr[i] > arr[j]:
                dup[d] = arr[j]
                j += 1
            else:
                dup[d] = arr[i]
                i += 1
        d += 1
    i, j = first, 0
    while i < last:
        arr[i] = dup[j]
        i += 1
        j += 1


def divide(arr, dup, first, last):
    if last - first <= 1:
        return
    mid = first + (last - first) / 2
    divide(arr, dup, first, mid)
    divide(arr, dup, mid, last)
    # to do
    merge(arr, dup, first, mid, last)


# DSU minimum version
class DSU:
    def __init__(self, n):
        self.parent = []
        for i in range(0, n):
            self.parent.append(i)

    def find(self, x):
        if x != self.parent[x]:
            self.parent[x] = self.find(self.parent[x])
        return self.parent[x]

    def union(self, x, y):
        xp, yp = self.find(x), self.find(y)
        if xp == yp:
            return False
        self.parent[yp] = xp
        return True


# Trie
class Trie:
    def __init__(self):
        """
        use '#' as count
        """
        self.nodes = {}

    def insert(self, word):
        node = self.nodes
        for c in word:
            if c not in node:
                node[c] = {}
            node = node[c]
        node['#'] = 1 + node.get('#', 0)  # default=0

    def search(self, word):
        node = self.nodes
        for c in word:
            if c not in node:
                return False
            node = node[c]
        return node.get('#') > 0

    def startsWith(self, word):
        node = self.nodes
        for c in word:
            if c not in node:
                return False
            node = node[c]
        return True


# Fenwick tree(BIT)
class Fenwick:
    def __init__(self, n):
        self.tree = [0] * (n + 1)

    def sum(self, index):
        res = 0
        index += 1
        while index > 0:
            res += self.tree[index]
            index -= (index & -index)
        return res

    def update(self, index, delta):
        N = len(self.tree)
        index += 1
        while index < N:
            self.tree[index] += delta
            index += (index & -index)

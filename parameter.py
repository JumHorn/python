# _*_ encoding=utf8 _*-

# 和C语言指针参数传递类似，改变内部指向，外面的值不变，改变内部的参数，外面的值改变
# 不可变参数通过值传递，可变参数通过指针传递
def change(l):
    l[2] = (1, 2, 3)
    print(l)


l = [1, 2, 3]
print(l)
change(l)
print(l)

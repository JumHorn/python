# -*- coding: UTF-8 -*-

import requests
import filemd5  # for md5
import os  # for file size

# get
# r = requests.get("https://www.baidu.com")

# register
# info = {
#     'userName': 'JumHorn',
#     'phone': '18750201580',
#     'firstPwd': 'xxx',
#     'nickName': 'Jum',
#     'email': 'JumHorn@gmail.com'
# }

# r = requests.post(url='http://81.69.226.177/register',
#   data=json.dumps(info), verify=False)

# r = requests.post(url='http://81.69.226.177/register', json=info, verify=False)

# print(r.status_code)
# print(r.text)


# login
def login(user, passwd):
    info = {}
    info["user"] = user
    info["pwd"] = passwd
    r = requests.post(url='http://81.69.226.177/login',
                      json=info,
                      verify=False)
    print(r.status_code)
    print(r.text)


# upload file with md5 check
def checkfile(user, token, filename):
    info = {}
    info["user"] = user
    info["token"] = token
    info["md5"] = filemd5.getfilemd5(filename)
    info["fileName"] = filename
    r = requests.post(url='http://81.69.226.177/md5', json=info, verify=False)
    print(r.status_code)
    print(r.text)


# upload file
def uploadfile(user, filename):
    boundary = "------WebKitFormBoundary88asdgewtgewx\r\n"

    data = ""
    # boundary
    data += boundary
    data += "\r\n"
    # 上传文件信息
    data += "Content-Disposition: form-data; "
    data += "user=\"%s\" " % (user)
    data += "filename=\"%s\" " % (filename)
    data += "md5=\"%s\" " % (filemd5.getfilemd5(filename))
    data += "size=\"%d\" " % (os.path.getsize("thread.py"))
    data += "Content-Type: application/octet-stream"
    data += "\r\n"
    data += "\r\n"

    # 上传文件内容
    f = open(filename, 'rb')

    data += f.read()
    data += "\r\n"

    # 结束分隔符
    data += boundary

    # 发送请求
    r = requests.post(url='http://81.69.226.177/upload',
                      data=data,
                      verify=False)
    print(r.status_code)
    print(r.text)


# 获取用户文件数量
def getuserfilecount(user, token):
    info = {}
    info["user"] = user
    info["token"] = token
    r = requests.post(url='http://81.69.226.177/myfiles?cmd=count',
                      json=info,
                      verify=False)
    print(r.status_code)
    print(r.text)


# 获取用户文件信息
def getuserfileinfo(user, token):
    info = {}
    info["user"] = user
    info["token"] = token
    info["start"] = 0
    r = requests.post(url='http://81.69.226.177/myfiles?cmd=normal',
                      json=info,
                      verify=False)
    print(r.status_code)
    print(r.text)


# 分享文件
def shareuserfile(user, token, filename):
    info = {}
    info["user"] = user
    info["token"] = token
    info["md5"] = filemd5.getfilemd5(filename)
    info["fileName"] = filename

    r = requests.post(url='http://81.69.226.177/dealfile?cmd=pv',
                      json=info,
                      verify=False)
    print(r.status_code)
    print(r.text)


if __name__ == '__main__':
    # login("JumHorn", "xxx")
    # checkfile("JumHorn", "7ed4f7bdc28a3db3205b9f1ee060786d", "thread.py")
    # uploadfile("JumHorn", "main.py")
    # getuserfilecount("JumHorn", "7ed4f7bdc28a3db3205b9f1ee060786d")
    # getuserfileinfo("JumHorn", "7ed4f7bdc28a3db3205b9f1ee060786d")
    shareuserfile("JumHorn", "7ed4f7bdc28a3db3205b9f1ee060786d", "thread.py")

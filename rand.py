# -*- coding: UTF-8 -*-

# 从module中导入任何函数，所以直接调用函数
from random import *

'''
# 导入模块 调用时先用模块名再用函数名
import random
# 例子
random.random()
# 例子
import tensorflow as tf
tf.VERSION
'''

if __name__ == "__main__":
    count = int(input("输入需要的随机数个数\n"))
    for i in range(0, count):
        while True:
            rand = int(random() * 1000000)
            if rand <= 688981 and rand >= 688001:
                print("%06d" % (rand))
                break
            if rand <= 605599 and rand >= 605001:
                print("%06d" % (rand))
                break
            if rand <= 603999 and rand >= 603000:
                print("%06d" % (rand))
                break
            if rand <= 601999 and rand >= 601000:
                print("%06d" % (rand))
                break
            if rand <= 600999 and rand >= 600000:
                print("%06d" % (rand))
                break
            if rand <= 301298 and rand >= 301000:
                print("%06d" % (rand))
                break
            if rand <= 3043 and rand >= 1:
                print("%06d" % (rand))
                break

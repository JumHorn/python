# -*- coding: UTF-8 -*-


from datetime import datetime, date, time, timedelta
import calendar

print(datetime.now())
print(datetime.utcnow())


# calc day of year
today = date.today()
day_of_year = (today - date(today.year, 1, 1)).days + 1
print(day_of_year)


# add days
tomorrow = today + timedelta(days=1)
print(tomorrow)


# modify day
today = today.replace(day=12)

# leap year
print(calendar.isleap(today.year))
